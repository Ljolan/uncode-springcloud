﻿//@ sourceURL=tasklist.js
layui.config({
    base: '../../lib/' //指定 winui 路径
    , version: '1.0.0-beta'
}).extend({
    winui: 'winui/winui',
    window: 'winui/js/winui.window'
}).define(['table', 'jquery', 'winui', 'window', 'layer'], function (exports) {

    winui.renderColor();

    var table = layui.table,
        $ = layui.$, tableId = 'tableid';
    //桌面显示提示消息的函数
    var msg = top.winui.window.msg;
    //表格渲染
    table.render({
        id: tableId,
        elem: '#tasklist',
        url: '/task/list',
        page: true,
        limits: [10, 20, 30, 40, 50, 60, 70, 100],
        limit: 10,
        cols: [[
            { field: 'id', type: 'checkbox' },
            { field: 'targetBean', title: '目标bean', width: 120 },
            { field: 'targetMethod', title: '目标方法', width: 120 },
            { field: 'cronExpression', title: 'cronExpression', width: 120 },
            { field: 'startTime', title: '开始时间', width: 120},
            { field: 'period', title: 'period', width: 120 },
            { field: 'delay', title: 'delay', width: 120 },
            { field: 'status', title: '状态', width: 120, templet: '#status'},
            { field: 'type', title: '类型', width: 120 },
            { field: 'runTimes', title: '执行次数', width: 120 },
            { field: 'lastRunningTime', title: '最次执行时间', width: 120 },
            { field: 'message', title: '执行信息', width: 120 },
            { field: 'percentage', title: '完成比例', width: 120 },
            { field: 'lastRunningTime', title: '最次执行时间', width: 120 },
            { field: 'params', title: '参数', width: 120 },
            
            { field: 'extKeySuffix', title: 'extKeySuffix', width: 120 },
            { field: 'beforeMethod', title: 'beforeMethod', width: 120 },
            { field: 'afterMethod', title: 'afterMethod', width: 120 },
            { field: 'threadNum', title: 'threadNum', width: 120 },
            { field: 'subSuffix', title: 'subSuffix', width: 120 },
            { field: 'subParams', title: 'subParams', width: 120 },
            { field: 'currentServer', title: '当前执行节点', width: 120 },
            { title: '操作', fixed: 'right', align: 'center', toolbar: '#barMenu', width: 150 }
        ]]
    });
    //监听工具条
    table.on('tool(taskListTable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        var ids = '';   //选中的Id
        $(data).each(function (index, item) {
            ids += item.id + ',';
        });
        if (layEvent === 'del') { //删除
            deleteMenu(ids, data);
        } else if (layEvent === 'edit') { //编辑
            openEditWindow(data);
        }
    });
    //监听单元格编辑
    table.on('edit(taskListTable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        if (/^[0-9]+$/.test(obj.value)) {
            var index = layer.load(1);
            $.ajax({
                type: 'post',
                url: '/task/updateOrder',
                data: { "id": obj.data.id, "order": obj.value },
                success: function (json) {
                    layer.close(index);
                    if (!json.success) {
                        msg(json.message);
                    }
                },
                error: function (xml) {
                    layer.close(index);
                    msg("修改失败", {
                        icon: 2,
                        time: 2000
                    });
                    console.log(xml.responseText);
                }
            });
        }
    });
    //打开编辑窗口
    function openEditWindow(tdata) {
        if (!tdata) return;
        var content;
        var index = layer.load(1);
        $.ajax({
            type: 'get',
            url: 'edit.html?id=' + tdata.id,
            success: function (data) {
                layer.close(index);
                content = data;
                //从桌面打开
                top.winui.window.open({
                    id: 'edittask',
                    type: 1,
                    title: '编辑灰度规则',
                    params:tdata,
                    content: content,
                    area: ['50vw', '70vh'],
                    offset: ['15vh', '25vw'],
                });
            },
            error: function (xml) {
                layer.close(index);
                msg("获取页面失败", {
                    icon: 2,
                    time: 2000
                });
                console.log(xml.responseText);
            }
        });
    }
    //删除菜单
    function deleteMenu(ids, obj) {
        var mg = obj ? '确认删除菜单【' + obj.data.targetBean + "-" + obj.data.targetMethod + '】吗？' : '确认删除选中数据吗？';
        top.winui.window.confirm(mg, { icon: 3, title: '删除菜单' }, function (index) {
            layer.close(index);
            
            $.ajax({
                type: 'get',
                url: '/task/delete',
                data: { "targetBean": obj.data.targetBean, 
                		"targetMethod": obj.data.targetMethod, 
                		"extKeySuffix": obj.data.extKeySuffix
                },
                success: function (json) {
                	if (json.success) {
                        msg('修改成功', {
                            icon: 1,
                            time: 2000
                        });
                        //刷新表格
                        if (obj) {
                            obj.del(); //删除对应行（tr）的DOM结构
                        } else {
                            reloadTable();  //直接刷新表格
                        }
                    } else {
                        msg(json.message);
                    }
                },
                error: function (xml) {
                    layer.close(index);
                    msg("获取页面失败", {
                        icon: 2,
                        time: 2000
                    });
                    console.log(xml.responseText);
                }
            });
        });
    }
    //表格刷新
    function reloadTable() {
        table.reload(tableId, {});
    }
    
    
    //绑定工具栏添加按钮事件
    $('#addMenu').on('click', function () {
        var content;
        var index = layer.load(1);
        $.ajax({
            type: 'get',
            url: 'add.html',
            success: function (data) {
                layer.close(index);
                content = data;
                //从桌面打开
                top.winui.window.open({
                    id: 'addMenu',
                    type: 1,
                    title: '新增菜单',
                    content: content,
                    area: ['50vw', '70vh'],
                    offset: ['15vh', '25vw']
                });
            },
            error: function (xml) {
                layer.close(load);
                msg('操作失败', {
                    icon: 2,
                    time: 2000
                });
                console.error(xml.responseText);
            }
        });
    });
    
    //绑定工具栏编辑按钮事件
    $('#editMenu').on('click', function () {
        var checkStatus = table.checkStatus(tableId);
        var checkCount = checkStatus.data.length;
        if (checkCount < 1) {
            msg('请选择一条数据', {
                time: 2000
            });
            return false;
        }
        if (checkCount > 1) {
            msg('只能选择一条数据', {
                time: 2000
            });
            return false;
        }
        openEditWindow(checkStatus.data[0]);
    });
  
    $('#home').on('click', function () {
    	location.href = "homepage.html";
    });
    //绑定工具栏刷新按钮事件
    $('#reloadTable').on('click', reloadTable);

    exports('tasklist', {});
});
