package cn.uncode.springcloud.starter.boot.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;

import cn.uncode.springcloud.starter.boot.app.AppInfo;
import cn.uncode.springcloud.utils.obj.SpringUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 配置类
 *
 * @author Juny
 */
@Slf4j
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class EnvironmentAutoConfiguration{
	
	@Autowired
	private Environment env;
	

	@Bean
	public AppInfo appInfo() {
		AppInfo.setEnv(env);
		log.info("===Uncode-starter===EnvironmentAutoConfiguration===>AppInfo Environment registed ...");
		return AppInfo.me();
	}
	
	
	/**
	 * Spring上下文缓存
	 *
	 * @return SpringUtil
	 */
	@Bean
	public SpringUtil springUtils() {
		log.info("===Uncode-starter===EnvironmentAutoConfiguration===>SpringUtil inited...");
		return new SpringUtil();
	}
	
    
    



}
