package cn.uncode.springcloud.starter.canary.api;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;

@Data
public class StrategyModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String flag;
	/**
	 * 匹配类型
	 */
	private String type;
	/**
	 * 匹配字段名称
	 */
	private String key;
	/**
	 * 匹配字段值列表
	 */
	private Set<String> value;

}
